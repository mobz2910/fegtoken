import os
import redis
class BaseConfig(object):
    CACHE_TYPE = 'Redis'
    CACHE_REDIS_HOST = 'localhost'
    CACHE_REDIS_PORT = '6379'
    #CACHE_REDIS_DB = os.environ['CACHE_REDIS_DB']
    #CACHE_REDIS_URL = os.environ['CACHE_REDIS_URL']