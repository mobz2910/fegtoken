import json, requests
from web3 import Web3
import time

"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""

def get_Amount_Token(tokenAddress, contractAddress, chainApi, chainUrl):

    web3 = Web3(Web3.HTTPProvider(chainUrl))  # connect to bsc node

    contract_address = web3.toChecksumAddress(tokenAddress)
    API_ENDPOINT = chainApi + "?module=contract&action=getabi&address=" + str(contract_address)
    r_fegtoken = requests.get(url=API_ENDPOINT)
    response = r_fegtoken.json()
    abi = json.loads(response["result"])

    contract = web3.eth.contract(address=contract_address, abi=abi)
    address = web3.toChecksumAddress(contractAddress)
    balance = contract.functions.balanceOf(address).call()
    time.sleep(5)
    return balance