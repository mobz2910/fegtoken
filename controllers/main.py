from flask import Flask
from flask_caching import Cache
from controllers.feg.fegController import fegController
from controllers.token.tokenController import tokenController
from tools import configRedis
import redis
"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""
#config = {
#    "DEBUG": True,
#    "CACHE_TYPE": "SimpleCache",
#}
#REDIS_URL = "redis://localhost:6379/0"

app = Flask(__name__)

app.register_blueprint(fegController, url_prefix='/feg')
app.register_blueprint(tokenController, url_prefix='/token')

#print(app.url_map)
