from models.feg.liquidityPool import LiquidityPoolMongoDb

"""
    --------------------------------------------
    [S]earch
    [C]reate
    [R]ead
    [U]pdate
    [D]elete
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""


class ServiceMongodbFegLiquidityPool:
    """
        ---------------
        CREATE PART
        ---------------
    """

    @classmethod
    def create_liquidity_pools(cls, db, liquidityPool: LiquidityPoolMongoDb):
        db.insert(liquidityPool.__dict__)
        return cls.get_liquidityPool_by_name(db, liquidityPool.name)

    """
        ---------------
        READ PART
        ---------------
    """

    @classmethod
    def get_all_liquidity_pools(cls, db):
        liquidityPools_list = []
        for liquidityPool in db.find():
            liquidityPools_list.append(LiquidityPoolMongoDb.JsonDecoder(liquidityPool))
        return liquidityPools_list

    @classmethod
    def get_liquidityPool_by_name(cls, db, name):
        liquidityPools_list = []
        for liquidityPool in db.find(key='name', value=name):
            liquidityPools_list.append(LiquidityPoolMongoDb.JsonDecoder(liquidityPool))
        return liquidityPools_list[0]

    @classmethod
    def get_liquidityPool_by_swapContract(cls, db, swapContract):
        tokens_list = []
        for address in db.find(key='swapContract', value=swapContract):
            tokens_list.append(LiquidityPoolMongoDb.JsonDecoder(address))
        return tokens_list[0]

    """
        ---------------
        UPDATE PART
        ---------------
    """

    @classmethod
    def update_token(cls, db, liquidityPool: LiquidityPoolMongoDb, updated_liquidityPool: LiquidityPoolMongoDb):
        cls.delete_liquidityPool(db, liquidityPool)
        return cls.create_token(db, updated_liquidityPool)

    """
        ---------------
        DELETE PART
        ---------------
    """
    @classmethod
    def delete_liquidityPool(cls, db, liquidityPool: LiquidityPoolMongoDb):
        return db.delete(key='name', value=liquidityPool.name)