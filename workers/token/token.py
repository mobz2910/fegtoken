import redis
from models.feg.token import TokenMongoDb
import json, requests
from web3 import Web3
from services.feg.token import ServiceMongodbFegToken
from tools import mongodbtoken, binanceapi
from settings.globalSettings import CONST

"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""


class tokenWorker:
    @classmethod
    def create_token_bsc(cls, newtoken):
        db = mongodbtoken.Tokendb(
            url=CONST.PWD_GET('DATABASE', 'url'), #https://bsc-dataseed.binance.org/
            port=CONST.PWD_GET('DATABASE', 'port') #https://api.bscscan.com/api
        )
        #take from here
        web3 = Web3(Web3.HTTPProvider(CONST.PWD_GET('URLS', 'bscurl')))  # connect to bsc node

        contract_address = web3.toChecksumAddress(newtoken.address) #address of the contract (fbnb, fegbsc, fegeth...)
        API_ENDPOINT = CONST.PWD_GET('URLS', 'bscapi') + "?module=contract&action=getabi&address=" + str(contract_address)
        r_fegtoken = requests.get(url=API_ENDPOINT)
        response = r_fegtoken.json()
        abi = json.loads(response["result"]) #abi is the json file that describes all the functions that a contract can perform
        #to here then go to get_token_amount
        token = TokenMongoDb()
        token.name = newtoken.name
        token.address = newtoken.address
        token.decimals = newtoken.decimals
        token.abi = json.dumps(abi)
        result = ServiceMongodbFegToken.create_token(db, token)
        if result is not None:
            return 'ok'
        else:
            return 'object not created'

    @classmethod
    def get_token_amount(cls, nameToken, swapContract):
        db = mongodbtoken.Tokendb(
            url=CONST.PWD_GET('DATABASE', 'url'),
            port=CONST.PWD_GET('DATABASE', 'port')
        )

        web3 = Web3(Web3.HTTPProvider(CONST.PWD_GET('URLS', 'bscurl')))
        try:
            token = ServiceMongodbFegToken.get_token_by_name(db, nameToken)
            abi = json.loads(token.abi)
            #take here
            addr = Web3.toChecksumAddress(token.address)
            contract = web3.eth.contract(address=addr, abi=abi)
            address = web3.toChecksumAddress(swapContract)
            balance = contract.functions.balanceOf(address).call()
            decimals = token.decimals-1
            balance = balance / 10**decimals #decimals is in the description of the contract on bsc
            return balance
            #to here then repeat operation for the second token in the lp. Then go to get_spot_price in the else statement
        except Exception as e:
            return e

    @classmethod
    def get_spot_price(cls, nameTokenIn, nameTokenOut, swapContract):
        redisdb = redis.Redis('localhost')
        spot_price = redisdb.get(nameTokenIn+'_'+nameTokenOut)
        if spot_price is not None:
            return spot_price
        else:
            fegAmount = cls.get_token_amount(nameTokenIn, swapContract)
            fbnbAmount = cls.get_token_amount(nameTokenOut, swapContract)
            #from here
            spot_price = fbnbAmount / fegAmount
            bnb_price = binanceapi.get_bnb_price()
            spot_price = float(spot_price) * float(bnb_price)
            #to here
            redisdb.setex(nameTokenIn+'_'+nameTokenOut, 10, value=str(spot_price))
            return (str(spot_price))
