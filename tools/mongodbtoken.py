import json

import pymongo
from bson.json_util import dumps

from tools.colors import bcolors

"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""

class Tokendb:
    def __init__(self, url, username=None, password=None, port="27017", dbname='fegtoken',
                 collection = 'token'):
        self.url = url
        self.username = username
        self.password = password
        self.port = port
        self.urlbase = "mongodb://{url}:{port}/".format(url=self.url, port=self.port)
        self.dbname = dbname
        self.collection = collection

    def open(self):
        """
        Log in to Mongodb with info provided during class instanciation
        :return: Open MyCollection
        """
        myclient = pymongo.MongoClient(self.urlbase)
        mydb = myclient[self.dbname]
        mycol = mydb[self.collection]

        return mycol, myclient

    def close(self, myclient):
        return myclient.close()

    def insert(self, data):
        mycol, myclient = self.open()

        result = mycol.insert_one(data)
        print('----------------------------------------------------------------------------------------------------')
        print(f'{bcolors.OKBLUE}[JSON DATA]      -   {data}')
        print('----------------------------------------------------------------------------------------------------')
        print(f'{bcolors.OKBLUE}[MONGODB OBJECT]      -   {result}')
        print('----------------------------------------------------------------------------------------------------')
        myclient.close()

        return result

    def find(self, key=None, value=None):
        mycol, myclient = self.open()
        if key == None and value == None:
            results = mycol.find()
        else:
            list_result = []
            myquery = {f"{key}": f"{value}"}
            list_result.append(json.loads(dumps(mycol.find_one(myquery))))
            return list_result
            #results = mycol.find_one(myquery)
            #return list(json.loads(dumps(results)))
        myclient.close()
        return json.loads(dumps(results))

    def delete(self, key, value):
        mycol, myclient = self.open()
        myquery = {f"{key}": f"{value}"}
        result = mycol.delete_one(myquery)
        myclient.close()
        return result
