import json
"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""

class LiquidityPoolMongoDb:

    def __init__(self, name=None, nameTokenIn=None, nameTokenOut=None, swapContract=None):
        self.name = name
        self.nameTokenIn = nameTokenIn
        self.nameTokenOut = nameTokenOut
        self.swapContract = swapContract

    def JsonDecoder(obj):
        return LiquidityPoolMongoDb(
            obj.get('name', None),
            obj.get('nameTokenIn', None),
            obj.get('nameTokenOut', None),
            obj.get('swapContract', None)
        )