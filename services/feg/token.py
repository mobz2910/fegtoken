from models.feg.token import TokenMongoDb

"""
    --------------------------------------------
    [S]earch
    [C]reate
    [R]ead
    [U]pdate
    [D]elete
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""


class ServiceMongodbFegToken:
    """
        ---------------
        CREATE PART
        ---------------
    """

    @classmethod
    def create_token(cls, db, token: TokenMongoDb):
        db.insert(token.__dict__)
        return cls.get_token_by_name(db, token.name)

    """
        ---------------
        READ PART
        ---------------
    """

    @classmethod
    def get_all_tokens(cls, db):
        tokens_list = []
        for token in db.find():
            tokens_list.append(TokenMongoDb.JsonDecoder(token))
        return tokens_list

    @classmethod
    def get_token_by_name(cls, db, name):
        tokens_list = []
        for token in db.find(key='name', value=name):
            tokens_list.append(TokenMongoDb.JsonDecoder(token))
        return tokens_list[0]

    @classmethod
    def get_token_by_address(cls, db, address):
        tokens_list = []
        for address in db.find(key='address', value=address):
            tokens_list.append(TokenMongoDb.JsonDecoder(address))
        return tokens_list[0]

    """
        ---------------
        UPDATE PART
        ---------------
    """

    @classmethod
    def update_token(cls, db, token: TokenMongoDb, updated_token: TokenMongoDb):
        cls.delete_token(db, token)
        return cls.create_token(db, updated_token)

    """
        ---------------
        DELETE PART
        ---------------
    """
    @classmethod
    def delete_token(cls, db, token: TokenMongoDb):
        return db.delete(key='name', value=token.name)