from flask import Blueprint, abort
from workers.token.token import tokenWorker


"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""

fegController = Blueprint('fegController', __name__)


@fegController.route('/<nameTokenIn>/<nameTokenOut>/<swapContract>', methods=['GET'])
#@cache.cached(timeout=10, query_string=True)
def get_feg_bnb_spot_price(nameTokenIn, nameTokenOut, swapContract):
    try:
        return tokenWorker.get_spot_price(nameTokenIn, nameTokenOut, swapContract)
    except ConnectionError:
        abort(404)
"""
@fegController.route('')
def index():
    return redis_client.get('potato')
"""