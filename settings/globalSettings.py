import sys
import os
import configparser

"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""

class Settings:
    def __init__(self):
        self.config_pwd = configparser.ConfigParser(interpolation=None)
        self.config_pwd.read(os.getcwd() + "\\settings\\secrets\\pwd.ini")

    def PWD_GET(self, name, key):
        return self.config_pwd.get(name, key)



global CONST
CONST = Settings()