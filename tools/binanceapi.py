from settings.globalSettings import CONST
from binance.client import Client
api_key = CONST.PWD_GET('BINANCE', 'api_key')
secret_key = CONST.PWD_GET('BINANCE', 'api_secret')

def get_bnb_price():
    client = Client(api_key, secret_key)
    return client.get_symbol_ticker(symbol='BNBUSDT')["price"]