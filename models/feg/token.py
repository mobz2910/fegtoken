import json
"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""

class TokenMongoDb:

    def __init__(self, name=None, address=None, decimals=None, abi=None):
        self.name = name
        self.address = address
        self.decimals = decimals
        self.abi = abi

    def JsonDecoder(obj):
        return TokenMongoDb(
            obj.get('name', None),
            obj.get('address', None),
            obj.get('decimals', None),
            obj.get('abi', None)
        )