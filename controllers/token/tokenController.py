from flask import Blueprint, request, abort
from models.feg.token import TokenMongoDb
from requests import ConnectionError
from workers.token.token import tokenWorker

"""
    --------------------------------------------
    Powered By akamob@fegtoken © 2021
    --------------------------------------------
"""

tokenController = Blueprint('tokenController', __name__)


@tokenController.route('/', methods=['POST'])
def create_token():
    try:
        new_token: TokenMongoDb = TokenMongoDb.JsonDecoder(request.get_json())
        return tokenWorker.create_token_bsc(new_token)
    except ConnectionError:
        abort(404)